# 1. Linux常用网络命令

## 1.1 hostname
```
    -d: 显示机器所属域名
    -f: 显示完整的主机名和域名
    -i: 显示当前机器的ip地址
```
~~~
    # hostname
    temp01.bg.zll.qianxin-inc.cn
    # hostname -d
    bg.zll.qianxin-inc.cn
    # hostname -f
    temp01.bg.zll.qianxin-inc.cn
    # hostname -i
    10.44.99.74
~~~

## 1.2 nslookup
> nslookup可以查找给定域名的所有ip地址


## 1.3 netstat
> 该命令用于显示各种网络相关信息，如网络连接，路由表，接口状态，连接，多播成员等等。

&nbsp;&nbsp;&nbsp;&nbsp;netstat的输出结果可以分为两个部分：
- 一个是Active Internet connections，称为有源TCP连接，其中"Recv-Q"和"Send-Q"指%0A的是接收队列和发送队列。
这些数字一般都应该是0。如果不是则表示软件包正在队列中堆积。
- 另一个是Active UNIX domain sockets，称为有源Unix域套接口(和网络套接字一样，但是只能用于本机通信，性能可以提高一倍)。 

**常用参数：**
~~~
    -a: 显示所有选项，默认不显示LISTEN相关
    -t: 仅显示tcp相关选项
    -u: 仅显示udp相关选项
    -n: 拒绝显示别名，能显示数字的全部转化成数字
    -p: 显示建立相关连接的程序名
    -x: 显示所有与Unix域相关的套接字选项
    -l: 仅列出有在 Listen (监听) 的服务状态
~~~
**_hint: LISTEN和LISTENING的状态只有用`-a`或者`-l`才能看到_**

**状态说明**
~~~
    LISTEN: 侦听来自远方的TCP端口的连接请求
    ESTABLISHED: 代表一个打开的连接
    TIME-WAIT: 等待足够的时间以确保远程TCP接收到连接中断请求的确认
    CLOSED: 没有任何连接状态
~~~
**_可参考tcp连接建立前的一次握手三报文传输过程_**
<hr>

> 1.列出所有tcp端口
~~~
$ netstat -at | more
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State
tcp        0      0 0.0.0.0:31208           0.0.0.0:*               LISTEN
tcp        0      0 localhost:10248         0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:30057           0.0.0.0:*               LISTEN
tcp        0      0 localhost:10249         0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:32650           0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:31882           0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:32331           0.0.0.0:*               LISTEN
tcp        0      0 localhost:26251         0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:31341           0.0.0.0:*               LISTEN
~~~

> 2.列出所有处于监听状态的Socket
~~~
$ netstat -l | more
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State
tcp        0      0 0.0.0.0:31208           0.0.0.0:*               LISTEN
tcp        0      0 localhost:10248         0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:30057           0.0.0.0:*               LISTEN
~~~

> 3.找出运行在指定端口的进程
~~~
$ netstat -an | grep ':80'
tcp        0      0 172.16.0.1:31701        172.16.0.2:8080         TIME_WAIT
tcp        0      0 172.16.0.1:31473        172.16.0.2:8080         TIME_WAIT
~~~

> 4.在netstat输出中显示 PID 和进程名称
~~~
$ netstat -tp | more
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 temp01.bg.zll.:postgres temp02.bg.zll.qia:60525 ESTABLISHED 25451/postgres: pos
tcp        0      0 temp01.bg.zll.:postgres temp02.bg.zll.qia:36819 ESTABLISHED 31079/postgres: pos
tcp        0      0 temp01.bg.zll.:postgres temp03.bg.zll.qia:19207 ESTABLISHED 1162/postgres: post
tcp        0      0 temp01.bg.zll.:postgres temp02.bg.zll:d2dconfig ESTABLISHED 20607/postgres: pos
tcp        0      0 temp01.bg.zll.:postgres temp03.bg.zll.qia:59353 ESTABLISHED 1173/postgres: post
tcp        0      0 temp01.bg.zll.:postgres temp02.bg.zll.qia:27961 ESTABLISHED 1782/postgres: post
~~~
**_hint:这里需要root权限才能看到PID/Program name字段的详细信息_**
