Git 密钥生成以及Gitlab配置
安装Git：详见http://www.cnblogs.com/xiuxingzhe/p/9300905.html

开通gitlab(开通需要咨询所在公司的gitlab管理员)账号后，本地Git仓库和gitlab仓库仍然不能传输项目，原因是要通过SSH加密才能传输，所以需要让gitlab认证本地的SSH Key

认证之前，则先使用Git生成SSH Key

## 1. Git生成密钥

### 1.1 确认本地密钥
```
SSH 秘钥默认储存在账户的主目录下的 ~/.ssh 目录
如：~/.ssh/
查看是否包含id_rsa和id_rsa.pub(或者是id_dsa和id_dsa.pub之类成对的文件)，有.pub 后缀的文件就是公钥，另一个文件则是密钥。
如果有这两个文件，则跳过1.2；如果没有这两个文件，甚至.ssh目录也没有，则需要用ssh-keygen 来创建
```

### 1.2 生成密钥信息
- 在.ssh目录下生成秘钥：ssh-keygen -t rsa -C "your_email@youremail.com" ，直接Enter就行，然后会提示输入密码(可输可不输)

## 2. gitlab秘钥添加

### 2.1 登录gitlab

### 2.2 添加秘钥
- 在搜索框中搜索：SSH Keys
- 点击 Add Keys
- 拷贝公钥文件（即id_rsa.pub)中的信息到key输入框中，title可以随便起，见名知意即可。
